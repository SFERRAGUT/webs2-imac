<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css"/>
    <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;900&display=swap" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/fontawesome.min.css" rel="stylesheet">
    <script src="https://kit.fontawesome.com/a076d05399.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
    <title>PROJIMAC</title>
</head>
<body>

<?php
    include('traitement/formulaireProjet.php');
?>

    <div id="partie-recherche">
        <div class="conteneur">
            <div id="structureRecherche">
                <div class="form-group has-search">
                    <span class="fa fa-search form-control-feedback"></span>
                    <input type="text" class="form-control" id="champRecherche" placeholder="Recherche parmi les noms de projets">
                </div>
            </div>
            <div id="structureBtnRetour">
                <div class="form-group has-search">
                    <h4>⮜ Revenir aux projets</h4>
                </div>
            </div>
        </div>
    </div>
    <div id="partieRappelProjet">
        <div class="conteneur">
            <div class="row">
                <div class="col-md-9" id="rappelProjet">
                </div>
            </div>
        </div>
    </div>

    <div id="creergroupe">
        <div class="conteneur">
            <div class="row">
                <div class="col-md-9">
                    <div id="structureBtnCreerGroupe" data-toggle="modal" data-target="#modalcreergroupe" data-whatever="@getbootstrap">
                        <h4>Créer un groupe</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>


    <?php
    include('traitement/creerMembre.php');
    include('traitement/rejoindreGroupePopup.php');
    include('traitement/modifierProjetPopup.php');
    include('traitement/supprimerProjetPopup.php');
    ?>

    <div id="partieResultats">
        <div class="conteneur">
            <div class="row">
                <div class="col-md-9 order-sm-last order-md-first">
                    <div class="row" id="partieTri">
                        <div class="col-md-9">
                            <p>Résultats de la recherche : <span id="nbResult">0</span></p>
                        </div>
                        <div class="col-md-3">
                            <select class="custom-select mr-sm-2" id="triResultats">
                                    <option disabled selected>Trier par</option>
                                    <option value="dateAsc">Dates de rendu: croissant</option>
                                    <option value="dateDsc">Dates de rendu: décroissant</option>
                                    <option value="enCours">Projets en cours</option>
                                    <option value="termines">Projets terminés</option>
                            </select>
                        </div>
                    </div>

                    <div class="row" id="resultats">

                    </div>

                </div>
                <div class="col-md-3 order-md-last order-sm-first order-first filtrage">
                    <div id="filtresProjets">
                        <form id="formFiltreProjets">
                            <div id="selectCat">
                            </div>
                            <br>
                            <label class="form-check-label"> <span class="bold">Semestre</span></label><br>
                            <div class="form-check form-check-inline pastilles">
                                <input class="form-check-input" type="radio" name="semestreProjet" id="semestreProjet" value="all" checked>
                                <label class="form-check-label" for="semestreProjet">
                                    All
                                </label>
                            </div>
                            <div class="form-check form-check-inline pastilles">
                                <input class="form-check-input" type="radio" name="semestreProjet" id="semestreProjet1" value="1">
                                <label class="form-check-label" for="semestreProjet1">
                                    S1
                                </label>
                            </div>

                            <div class="form-check form-check-inline pastilles">
                                <input class="form-check-input" type="radio" name="semestreProjet" id="semestreProjet2" value="2">
                                <label class="form-check-label" for="semestreProjet2">
                                    S2
                                </label>
                            </div>
                            <div class="form-check form-check-inline pastilles">
                                <input class="form-check-input" type="radio" name="semestreProjet" id="semestreProjet3" value="3">
                                <label class="form-check-label" for="semestreProjet3">
                                    S3
                                </label>
                            </div>
                            <div class="form-check form-check-inline pastilles">
                                <input class="form-check-input" type="radio" name="semestreProjet" id="semestreProjet4" value="4">
                                <label class="form-check-label" for="semestreProjet4">
                                    S4
                                </label>
                            </div>
                            <div class="form-check form-check-inline pastilles">
                                <input class="form-check-input" type="radio" name="semestreProjet" id="semestreProjet5" value="5">
                                <label class="form-check-label" for="semestreProjet5">
                                    S5
                                </label>
                            </div>
                            <div class="form-check form-check-inline pastilles">
                                <input class="form-check-input" type="radio" name="semestreProjet" id="semestreProjet6" value="6">
                                <label class="form-check-label" for="semestreProjet6">
                                    S6
                                </label>
                            </div>
                            <br>
                            <button id="submit_filtres" class="btn btn-secondary">Filtrer</button>
                        </form>
                    </div>
                    <div id="filtreGroupes">
                        <form id="formFiltreGroupes">
                            <div id="selectGroupe">
                                <div class="form-check">
                                    <input class="form-check-input label-to-bold-if-checked" type="radio" name="categorieGroupe" id="categorieGroupe1" value="all" checked>
                                    <label class="form-check-label label-check" for="categorieGroupe1">
                                        Tous les Groupes
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input label-to-bold-if-checked" type="radio" name="categorieGroupe" id="categorieGroupe2" value="free">
                                    <label class="form-check-label label-check" for="categorieGroupe2">
                                        Incomplets
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input label-to-bold-if-checked" type="radio" name="categorieGroupe" id="categorieGroupe3" value="full">
                                    <label class="form-check-label label-check" for="categorieGroupe3">
                                        Complets
                                    </label>
                                </div>
                            </div>
                            <button id="submit_filtre_groupe" type ="button" class="btn btn-secondary">Filtrer</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>
    <script src="js/formulaire.js"></script>
    <script src="js/ajax.js"></script>
</html>
