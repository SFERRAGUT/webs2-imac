<?php
include('connexion.php');
$method=strtolower($_SERVER['REQUEST_METHOD']);

if($method == 'post'){
  $json = file_get_contents('php://input');
  $data = json_decode($json, TRUE);
  $tab = array();

  //$idProjet = $data['idProjet'];
  $idGroupe = $data['idGroupe'];
  //echo($idGroupe);

  $nombre=$bdd->prepare("SELECT nb_membre FROM groupe WHERE id_groupe = $idGroupe");
  $nombre->execute();
  $nombre = $nombre->fetch();

  $nbMembres = $nombre['nb_membre'];

  $verif=$bdd->prepare("SELECT count(id_eleve) FROM membre_d_un WHERE id_groupe= $idGroupe");
  $verif->execute();
  $verif = $verif->fetch();
  //echo(" la véritable taille est : ".$verif[0]."  ");
  if($nbMembres != $verif[0]){
    $nbMembres = $verif[0];
  }

  $Groupe=$bdd->prepare("SELECT * FROM projet JOIN groupe ON groupe.id_projet = projet.id_projet WHERE id_groupe = $idGroupe");
  $Groupe->execute();
  $Groupe = $Groupe->fetch();
  $idProjet = $Groupe['id_projet'];
  $tailleGroupeMax = $Groupe['nb_max'];
  $promotionGroupe = $Groupe['id_promo'];
  //echo("taille MAX : ".$tailleGroupeMax." taille actuel : ".$nbMembres."\n");

  $reponseGroupe=$bdd->prepare("SELECT *
    FROM eleve JOIN etudie_en ON etudie_en.id_eleve = eleve.id_eleve WHERE etudie_en.id_promo = '$promotionGroupe'
    AND contact NOT IN
    (SELECT contact FROM (eleve JOIN membre_d_un ON membre_d_un.id_eleve = eleve.id_eleve)
    JOIN groupe ON groupe.id_groupe = membre_d_un.id_groupe
    WHERE groupe.id_projet = '$idProjet')");
  $reponseGroupe->execute();
  $reponseGroupe = $reponseGroupe->fetchAll();
  foreach ($reponseGroupe as $eleve) {
    //echo("taille MAX : ".$tailleGroupeMax." taille actuel : ".$nbMembres."\n");
    array_push($tab, array(
      'contact' => $eleve['contact'],
      'nom' => $eleve['nom'],
      'prenom' => $eleve['prenom'],
      'nb' => $tailleGroupeMax - $nbMembres,
      'idProjet' => $idProjet,
    ));
  }

header('Content-Type: application/json; charset=UTF-8');
header('HTTP/1.1 200 OK');
$response = json_encode($tab);
echo $response;

}
else {
    http_response_code(404);
}

?>
