<div class="modal fade" id="modalModifierProjet" tabindex="-1" role="dialog" aria-labelledby="modalModifierProjet" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Modifier un projet</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <form id="modifierProjet">
          <div class="form-group row">
            <div class="col">
              <input type="text" class="form-control" placeholder="Nom du Projet" name="nomProjet" id="nomProjet2">
              <input type="hidden" id="idProjet2">
            </div>
          </div>

          <div class="form-group row">
            <div class="col">
              <textarea class="form-control" id="textAreaDescription2" placeholder="Description du Projet" rows="4"></textarea>
            </div>
          </div>
            
          <div class="form-group row">
            <div class="col-5">
              <div class="input-group">
                <div class="input-group-prepend">
                  <div class="input-group-text">Groupe de</div>
                </div>
                  <input type="number" min="0" class="form-control" placeholder="Groupe de" name="tailleGroupe" id="tailleGroupe2">
              </div>
            </div>

            <div class="col-7">
              <div class="input-group">
                <div class="input-group-prepend">
                  <div class="input-group-text">Pour le</div>
                </div>
                  <input type="DATE" class="form-control" placeholder="Date de Rendu" name="dateRendu" id="dateRendu2">
              </div>
            </div>
          </div>


          <div class="form-group row">
            <div class="col">
              <div class="custom-file">
                <input type="url" class="form-control" id="fichierComplementaire2" placeholder="Lien vers des consignes complémentaires">
              </div>
            </div>
          </div>
        </form>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary" id="modifier" data-dismiss="modal">Modifier</button>
      </div>
    </div>
  </div>
</div>
