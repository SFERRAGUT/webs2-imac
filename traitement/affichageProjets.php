<?php
include('connexion.php');
$method=strtolower($_SERVER['REQUEST_METHOD']);

if($method == 'post'){
  $json = file_get_contents('php://input');
  $data = json_decode($json, TRUE);
  $recherche = $data['recherche'];
  $tri = $data['tri'];
  $semestre = $data['semestre'];
  $matiere = $data['matiere'];
  $idProjet = $data['idProjet'];
  $inGroup = false;

  if($idProjet != NULL){
    $projets=$bdd->prepare("SELECT* FROM projet WHERE id_projet = '$idProjet'");
    $inGroup = true;
  }
  else{
    if($matiere != ''){
      if($tri == "dateAsc"){
        $projets=$bdd->prepare("SELECT* FROM projet WHERE nom LIKE '%$recherche%' AND semestre LIKE '%$semestre%' AND id_matiere LIKE '$matiere' ORDER BY deadline ASC");
      }
      elseif($tri == "enCours"){
          $projets=$bdd->prepare("SELECT* FROM projet WHERE deadline > NOW() AND nom LIKE '%$recherche%' AND semestre LIKE '%$semestre%' AND id_matiere LIKE '$matiere' ORDER BY deadline");
      }
    
      elseif($tri == "termines"){
          $projets=$bdd->prepare("SELECT* FROM projet WHERE deadline <= NOW() AND nom LIKE '%$recherche%' AND semestre LIKE '%$semestre%' AND id_matiere LIKE '$matiere' ORDER BY deadline");
      }
      else{
          $projets=$bdd->prepare("SELECT* FROM projet WHERE nom LIKE '%$recherche%' AND semestre LIKE '%$semestre%' AND id_matiere LIKE '$matiere' ORDER BY deadline DESC");
      }
    }

    else{
      if($tri == "dateAsc"){
        $projets=$bdd->prepare("SELECT* FROM projet WHERE nom LIKE '%$recherche%' AND semestre LIKE '%$semestre%' ORDER BY deadline ASC");
      }
      elseif($tri == "enCours"){
          $projets=$bdd->prepare("SELECT* FROM projet WHERE deadline > NOW() AND nom LIKE '%$recherche%' AND semestre LIKE '%$semestre%' ORDER BY deadline");
      }
    
      elseif($tri == "termines"){
          $projets=$bdd->prepare("SELECT* FROM projet WHERE deadline <= NOW() AND nom LIKE '%$recherche%' AND semestre LIKE '%$semestre%' ORDER BY deadline");
      }
      else{
          $projets=$bdd->prepare("SELECT* FROM projet WHERE nom LIKE '%$recherche%' AND semestre LIKE '%$semestre%' ORDER BY deadline DESC");
      }
    }
  }
  

  $projets->execute();
  $projets = $projets->fetchAll();

  $i = 0;
  foreach ($projets as $projet){
    $id_projet = $projet['id_projet'];
    $icone=$bdd->prepare("SELECT icone FROM matiere JOIN projet ON projet.id_matiere = matiere.id_matiere WHERE projet.id_projet = '".$id_projet."'");
    $icone->execute();
    $icone = $icone->fetch();
    $icone = $icone['icone'];

    $promo =$bdd->prepare("SELECT * FROM projet JOIN promotion ON promotion.id_promo = projet.id_promo WHERE projet.id_projet = '".$id_projet."'");
    $promo->execute();
    $promo = $promo->fetch();
    $promo = $promo['nom'];

    $now = date('Y-m-d');
    $now = new DateTime( $now );
    $date = $projet['deadline'];
    $date= new DateTime( $date );
    $termine = false;

    if($date <=$now){
      $termine = true;
    }

    $type = $bdd->prepare("SELECT type FROM commanditaire JOIN projet ON commanditaire.id_commanditaire = projet.id_commanditaire WHERE projet.id_projet = '".$id_projet."'");
    $type->execute();
    $type = $type->fetch();

    if($type['type'] == "professeur"){
      $commanditaire = $bdd->prepare("SELECT nom_prof,contact FROM professeur JOIN commanditaire ON commanditaire.id_prof = professeur.id_prof JOIN projet ON commanditaire.id_commanditaire = projet.id_commanditaire WHERE projet.id_commanditaire = '".$projet['id_commanditaire']."'");
      $commanditaire->execute();
      $commanditaire = $commanditaire->fetch();
      $projets[$i] = (array(
        'id' => $id_projet,
        'nom' => $projet['nom'],
        'commanditaire' => $commanditaire['nom_prof'],
        'contact' => $commanditaire['contact'],
        'icone' => $icone,
        'semestre' => $projet['semestre'],
        'promo' => $promo,
        'deadline' => $projet['deadline'],
        'description' => $projet['description'],
        'nbEleves' => $projet['nb_max'],
        'pj' => $projet['pj'],
        'projetTermine' => $termine,
        ));
        $i++;
    }
    else if ($type['type'] ==  "eleve"){
      $commanditaire = $bdd->prepare("SELECT eleve.nom,contact FROM eleve JOIN commanditaire ON commanditaire.id_eleve = eleve.id_eleve JOIN projet ON commanditaire.id_commanditaire = projet.id_commanditaire WHERE projet.id_commanditaire = '".$projet['id_commanditaire']."'");
      $commanditaire->execute();
      $commanditaire = $commanditaire->fetch();
      $projets[$i] = (array(
        'id' => $id_projet,
        'nom' => $projet['nom'],
        'commanditaire' => $commanditaire['nom'],
        'contact' => $commanditaire['contact'],
        'icone' => $icone,
        'semestre' => $projet['semestre'],
        'promo' => $promo,
        'deadline' => $projet['deadline'],
        'description' => $projet['description'],
        'nbEleves' => $projet['nb_max'],
        'pj' => $projet['pj'],
        'projetTermine' => $termine,
        ));
        $i++;
    }



  }
  $reponse = (array(
    'inGroup'=> $inGroup,
    'projets' => $projets,
    ));

  echo json_encode($reponse);


  header('Content-Type: application/json; charset=UTF-8');
  header('HTTP/1.1 200 OK');
}
else {
    http_response_code(404);
}

?>
