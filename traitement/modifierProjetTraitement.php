<?php
include('connexion.php');
$method=strtolower($_SERVER['REQUEST_METHOD']);

if($method == 'post'){
    $json = file_get_contents('php://input');
    $data = json_decode($json, TRUE);
    $tab = array();

    $idProjet = $data['idProjet'];
    $nom = $data['nom'];
    $description = $data['description'];
    $nbMax = $data['nbMax'];
    $deadline = $data['deadline'];
    $pj = $data['pj'];

    //$projet=$bdd->prepare("UPDATE projet SET nom = '$nom', nb_max = '$nbMax', deadline = '$deadline', pj = '$pj' WHERE id_projet='$idProjet'");
    
    $projet = $bdd->prepare("UPDATE projet
    SET
        nom = ?,
        description = ?,
        nb_max = ?,
        deadline = ?,
        pj = ?
    WHERE
        id_projet= ?");
    
    $projet->bindParam(1, $nom, PDO::PARAM_STR);
    $projet->bindParam(2, $description, PDO::PARAM_STR);
    $projet->bindParam(3, $nbMax);
    $projet->bindParam(4, $deadline);
    $projet->bindParam(5, $pj, PDO::PARAM_STR);
    $projet->bindParam(6, $idProjet);
    $projet->execute();

    header('Content-Type: application/json; charset=UTF-8');
    header('HTTP/1.1 200 OK');
}
else {
    http_response_code(404);
}
?>