<?php
include('connexion.php');
$method=strtolower($_SERVER['REQUEST_METHOD']);

if($method == 'post'){
  $json = file_get_contents('php://input');
  $data = json_decode($json, TRUE);

  $idProjet = $data['idProjet'];
  $filtreGroupe = $data['filtreGroupe'];
  

  $tailleGroupeMax=$bdd->prepare("SELECT nb_max FROM projet WHERE id_projet = $idProjet");
  $tailleGroupeMax->execute();
  $tailleGroupeMax = $tailleGroupeMax->fetch();
  $tailleGroupeMax = $tailleGroupeMax['nb_max'];

  if($filtreGroupe == 'full'){
    $reponseGroupe=$bdd->prepare("SELECT id_groupe FROM groupe WHERE id_projet = $idProjet AND nb_membre = $tailleGroupeMax ");
  }
  else if($filtreGroupe == 'free'){
    $reponseGroupe=$bdd->prepare("SELECT id_groupe FROM groupe WHERE id_projet = $idProjet AND nb_membre < $tailleGroupeMax ");
  }

  else{
    $reponseGroupe=$bdd->prepare("SELECT id_groupe FROM groupe WHERE id_projet = $idProjet");
  }
   
  $reponseGroupe->execute();
  $reponseGroupe = $reponseGroupe->fetchAll();


  
  
  
  $i = 0;
  foreach ($reponseGroupe as $groupe){
    $groupe_id = $groupe['id_groupe'];
    $reponseGroupePart=$bdd->prepare("SELECT * FROM membre_d_un JOIN eleve ON membre_d_un.id_eleve = eleve.id_eleve WHERE id_groupe = $groupe_id");
    $reponseGroupePart->execute();
    $reponseGroupePart = $reponseGroupePart->fetchAll();

    $j = 0;
    foreach($reponseGroupePart as $eleve){
      $reponseGroupePart[$j] = (array(
        'id_eleve' => $eleve['id_eleve'],
        'nom' => $eleve['nom'],
        'prenom' => $eleve['prenom'],
        'contact' => $eleve['contact'],
        ));
        $j++;
    }

    $reponseGroupe[$i] = (array(
      'id_groupe' => $groupe_id,
      'membres' => $reponseGroupePart,
      ));
      $i++;
  }
  $reponse = (array(
    'id_projet' => $idProjet,
    'taille_max' => $tailleGroupeMax,
    'groupes' => $reponseGroupe,
    ));
 
  echo json_encode($reponse);


  header('Content-Type: application/json; charset=UTF-8');
  header('HTTP/1.1 200 OK');
}
else {
    http_response_code(404);
}

?>
