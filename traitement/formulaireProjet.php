<div id="partie-menu">
    <nav class="navbar navbar-light bg-light">
        <div class="conteneur">
            <a class="navbar-brand" href="#">
                <img src="media/PROJIMAC.png" height="38" alt="">
            </a>
        </div>
    </nav>
</div>

<div id="bandeau_formulaire">
    <div class="conteneur">
    <div class="row">
        <div class="col-md-6 align-self-center">
            <h2>Tous les projets et leurs groupes en un clic !</h2>
            <h3>Ça c’est <span class="black">IMAC</span>.</h3>
        </div>
        <div class="col-md-6">
            <form id="formulaireAjoutProjet" >
                <div class="form-group row">
                    <div class="col-md-9 col-sm-12">
                        <input type="text" class="form-control" placeholder="Nom du Projet" name="nomProjet" id="nomProjet" required>
                    </div>
                    <div class="col">
                        <select class="custom-select mr-sm-2" id="selectCategorie" required>
                            <option disabled selected>Catégorie</option>
                            <?php
                                    include('connexion.php');
                                    $bdd->query('Set names UTF8');
                                    $reponseCat1=$bdd->prepare('SELECT* FROM matiere ORDER BY nom_matiere');

                                    $reponseCat1->execute();
                                    while($donneesCat1 = $reponseCat1->fetch()){
                                        echo '<option value="' .$donneesCat1['tag']. '">'.$donneesCat1['nom_matiere'].'</option>';
                                    }
                                ?>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col">
                        <textarea class="form-control" id="textAreaDescription" placeholder="Description du Projet" rows="4" required></textarea>
                    </div>
                </div>

                <div class="form-group row">
                  <div class="col-3">
                      <select class="custom-select mr-sm-2" id="selectType" onchange="typeCommanditaire()" required>
                          <option disabled selected>Type</option>
                          <option value="professeur">Professeur</option>
                          <option value="eleve">Eleve</option>
                      </select>
                  </div>
                    <div class="col-6">
                        <select class="custom-select mr-sm-2" id="selectCommanditaire" required>
                            <optgroup label=Commanditaire>
                             <?php
                                $reponseCat3 = $bdd->prepare('SELECT * FROM commanditaire JOIN professeur ON professeur.id_prof = commanditaire.id_prof');
                                $reponseCat3->execute();
                                while($donneesCat3 = $reponseCat3->fetch()){
                                    echo '<option value ="'.$donneesCat3["contact"].'">'.$donneesCat3["nom_prof"].'</option>';
                                }
                                echo '</optgroup>';
                                echo '<optgroup label = "Nouveau commanditaire">';
                                echo '<optgroup label = "Enseignants">';

                                $reponseCat4 = $bdd->prepare('SELECT * FROM professeur WHERE id_prof NOT IN (select id_prof FROM commanditaire)');
                                $reponseCat4->execute();
                                while($donneesCat4 = $reponseCat4->fetch()){
                                    echo '<option value ="'.$donneesCat4["contact"].'">'.$donneesCat4["nom_prof"].'</option>';
                                }
                                echo '<optgroup label = "Élèves">';
                                $reponseEleve = $bdd->prepare('SELECT * FROM eleve ORDER BY nom');
                                $reponseEleve->execute();
                                while($donneesEleve = $reponseEleve->fetch()){
                                    echo '<option value ="'.$donneesEleve["contact"].'">'.$donneesEleve["nom"].' '.$donneesEleve["prenom"].'</option>';
                                }
                                echo '</optgroup></optgroup></optgroup>';
                            ?>
                        </select>
                    </div>
                    <div class="col">
                        <input type="number" min="0" class="form-control" placeholder="Groupe de" name="tailleGroupe" id="tailleGroupe" required>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-3 col-sm-6">
                        <select class="custom-select mr-sm-2" id="selectPromo">
                            <option disabled selected>Promo</option>
                            <?php
                                $reponseCat2 = $bdd->prepare('SELECT * FROM promotion');

                                $reponseCat2->execute();
                                while($donneesCat2 = $reponseCat2->fetch()){
                                    echo '<option value="'.$donneesCat2['nom'].'">'.$donneesCat2['nom'].'</option>';
                                }
                            ?>
                        </select>
                    </div>
                    <div class="col-md-3 col-sm-6">
                        <select class="custom-select mr-sm-2" id="selectSelect" required>
                            <option disabled selected>Semestre</option>
                            <optgroup label="IMAC 1">
                                <option value="S1">S1</option>
                                <option value="S2">S2</option>
                            </optgroup>
                            <optgroup label="IMAC 2">
                                <option value="S3">S3</option>
                                <option value="S4">S4</option>
                            </optgroup>
                            <optgroup label="IMAC 3">
                                <option value="S5">S5</option>
                                <option value="S6">S6</option>
                            </optgroup>
                        </select>
                    </div>
                    <div class="col">
                        <div class="input-group">
                            <div class="input-group-prepend">
                            <div class="input-group-text">Pour le</div>
                            </div>
                            <input type="DATE" class="form-control" placeholder="Date de Rendu" name="dateRendu" id="dateRendu" required>
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col">
                        <div class="custom-file">
                            <input type="url" class="form-control" id="fichierComplementaire" placeholder="Lien vers des consignes complémentaires">
                        </div>
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-sm-10">
                    <button id="button" type="submit" class="btn btn-primary">Valider</button>
                    </div>
                </div>

            </form>
        </div>
    </div>
    </div>
</div>
