<?php
include('connexion.php');
$method=strtolower($_SERVER['REQUEST_METHOD']);

if($method == 'post'){
    $json = file_get_contents('php://input');
    $data = json_decode($json, TRUE);
    $tab = array();

    $idProjet = $data['idProjet'];

    $projet=$bdd->prepare("SELECT * FROM projet WHERE id_projet = $idProjet");
    $projet->execute();
    $projet = $projet->fetch();

    $tab = array(
        'id' => $projet['id_projet'],
        'nom' => $projet['nom'],
        'nbMax' => $projet['nb_max'],
        'deadline' => $projet['deadline'],
        'semestre' => $projet['semestre'],
        'description' => $projet['description'],
        'pj' => $projet['pj'],
        'idCommanditaire' => $projet['id_commanditaire'],
        'idMatiere' => $projet['id_matiere'],
        'idPromo' => $projet['id_promo'],
    );

    header('Content-Type: application/json; charset=UTF-8');
    header('HTTP/1.1 200 OK');
    $response = json_encode($tab);
    echo $response;

}
else {
    http_response_code(404);
}

?>
