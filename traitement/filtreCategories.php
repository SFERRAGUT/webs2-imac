<?php
include('connexion.php');
// Affichage de l'ensemble des catégories
header("Content-Type: application/json; charset=UTF-8");

$bdd->query('Set names UTF8');
$reponse=$bdd->prepare('SELECT * FROM matiere ORDER BY nom_matiere');

$reponse->execute();
$reponseMatieres = $reponse->fetchAll();

$j = 0;
foreach ($reponseMatieres as $matiere){
    $reponseMatieres[$j] = (array(
        'nom' => $matiere['nom_matiere'],
        'id' => $matiere['id_matiere'],
        ));
        $j++;
}
$reponse = (array(
'matieres' => $reponseMatieres,
));

// response status
http_response_code(200);

echo json_encode($reponse);

exit();
?>