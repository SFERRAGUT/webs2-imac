<?php
include('connexion.php');
$method=strtolower($_SERVER['REQUEST_METHOD']);

if($method == 'post'){
  $json = file_get_contents('php://input');
  $data = json_decode($json, TRUE);
  $tab = array();

  $idProjet = $data['idProjet'];

  $Groupe=$bdd->prepare("SELECT * FROM projet WHERE id_projet = $idProjet");
  $Groupe->execute();
  $Groupe = $Groupe->fetch();
  $tailleGroupeMax = $Groupe['nb_max'];
  $promotionGroupe = $Groupe['id_promo'];

  $reponseGroupe=$bdd->prepare("SELECT *
    FROM eleve JOIN etudie_en ON etudie_en.id_eleve = eleve.id_eleve WHERE etudie_en.id_promo = '$promotionGroupe'
    AND contact NOT IN
    (SELECT contact FROM (eleve JOIN membre_d_un ON membre_d_un.id_eleve = eleve.id_eleve)
    JOIN groupe ON groupe.id_groupe = membre_d_un.id_groupe
    WHERE groupe.id_projet = '$idProjet')");
  $reponseGroupe->execute();
  $reponseGroupe = $reponseGroupe->fetchAll();
  foreach ($reponseGroupe as $eleve) {
    array_push($tab, array(
      'contact' => $eleve['contact'],
      'nom' => $eleve['nom'],
      'prenom' => $eleve['prenom'],
      'nb' => $tailleGroupeMax,
    ));
  }

header('Content-Type: application/json; charset=UTF-8');
header('HTTP/1.1 200 OK');
$response = json_encode($tab);
echo $response;

}
else {
    http_response_code(404);
}

?>
