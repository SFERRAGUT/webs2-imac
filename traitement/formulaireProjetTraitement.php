<?php

include('connexion.php');

$method=strtolower($_SERVER['REQUEST_METHOD']);

if($method == 'post'){
    $json = file_get_contents('php://input');
    $data = json_decode($json, TRUE);
    $tab = array();
    $typeCommanditaire = $data['typeCommanditaire'];

    if(strcmp($typeCommanditaire, 'professeur') == 0){
      $reponse = $bdd->prepare('SELECT * FROM professeur');
      $reponse->execute();
      $reponse = $reponse->fetchAll();

      foreach ($reponse as $professeur) {
        array_push($tab, array(
          'contact' => $professeur['contact'],
          'nom' => $professeur['nom_prof'],
        ));
      }
    }
    else if(strcmp($typeCommanditaire, 'eleve') == 0){
      $reponse = $bdd->prepare('SELECT * FROM eleve');
      $reponse->execute();
      foreach ($reponse as $eleve) {
        array_push($tab, array(
          'contact' => $eleve['contact'],
          'nom' => $eleve['nom'],
          'prenom' => $eleve['prenom'],
        ));
      }
    }

    header('Content-Type: application/json; charset=UTF-8');
    header('HTTP/1.1 200 OK');
    $response = json_encode($tab);
    echo $response;

}
else {
    http_response_code(404);
}

?>
