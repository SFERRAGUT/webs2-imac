<?php

$method=strtolower($_SERVER['REQUEST_METHOD']);

if($method == 'post'){
    $json = file_get_contents('php://input');
    $data = json_decode($json, TRUE);

    $nomProjet = $data['nomProjet'];
    $categorie = $data['categorie'];
    $description = $data['description'];
    $commanditaire = $data['commanditaire'];
    $tailleGroupe = $data['tailleGroupe'];
    $promo = $data['promo'];
    $semestre= $data['semestre'];
    $fichierComplementaire = $data['fichierComplementaire'];
    $date = $data['date'];
    $type = $data['type'];


    $response = json_encode(array(
    'nomProjet' => $nomProjet,
    'categorie' => $categorie,
    'description' => $description,
    'commanditaire' => $commanditaire,
    'tailleGroupe' => $tailleGroupe,
    'promo' => $promo,
    'semestre' => $semestre,
    'fichierComplementaire' => $fichierComplementaire,
    ));
    $description = addslashes($description);

    //$date= new DateTime( $date );
    //$date= date_format($date,'Y-m-d');

    include('connexion.php');
    $reponsePromo=$bdd->prepare("SELECT id_promo FROM promotion WHERE nom ='$promo' ");
    $reponsePromo->execute();
    $idPromo = $reponsePromo->fetch();
    $idPromo = $idPromo['id_promo'];

    $reponseMatiere=$bdd->prepare("SELECT id_matiere FROM matiere WHERE tag ='$categorie' ");
    $reponseMatiere->execute();
    $idMatiere = $reponseMatiere->fetch();
    $idMatiere = $idMatiere['id_matiere'];


    if(strcmp($semestre,'S1')==0){
      $numeroSemestre = 1;
    }
    if(strcmp($semestre,'S2')==0){
      $numeroSemestre = 2;
    }
    if(strcmp($semestre,'S3')==0){
      $numeroSemestre = 3;
    }
    if(strcmp($semestre,'S4')==0){
      $numeroSemestre = 4;
    }
    if(strcmp($semestre,'S5')==0){
      $numeroSemestre = 5;
    }
    if(strcmp($semestre,'S6')==0){
      $numeroSemestre = 6;
    }

    if(strcmp($type,'professeur')==0){
      $reponseCommanditaire=$bdd->prepare("SELECT id_commanditaire FROM commanditaire JOIN professeur ON commanditaire.id_prof = professeur.id_prof WHERE professeur.contact = '$commanditaire'");
      $reponseCommanditaire->execute();

      if(!($r2 = $reponseCommanditaire->fetch())){
        $r=$bdd->prepare("SELECT id_prof FROM  professeur WHERE professeur.contact = '$commanditaire'");
        $r->execute();
        $r1=$r->fetch();
        $id = $r1['id_prof'];
        $reponseAjoutCommanditaire=$bdd->prepare("INSERT INTO commanditaire (type, id_prof)
        VALUES ('professeur','$id')");
        $reponseAjoutCommanditaire->execute();

        $r3=$bdd->prepare("SELECT id_commanditaire FROM commanditaire JOIN professeur ON commanditaire.id_prof = professeur.id_prof WHERE professeur.contact = '$commanditaire'");
        $r3->execute();
        $r4 = $r3->fetch();
        $idCommanditaire = $r4['id_commanditaire'];

      }
      else{
        $idCommanditaire = $r2['id_commanditaire'];
      }

    }
    else if(strcmp($type,'eleve')==0){
      $reponseCommanditaire=$bdd->prepare("SELECT id_commanditaire FROM commanditaire JOIN eleve ON commanditaire.id_eleve = eleve.id_eleve WHERE eleve.contact = '$commanditaire'");
      $reponseCommanditaire->execute();

      if(!($r2=$reponseCommanditaire->fetch())){
        $r=$bdd->prepare("SELECT id_eleve FROM  eleve  WHERE eleve.contact = '$commanditaire'");
        $r->execute();
        $r1=$r->fetch();
        $id = $r1['id_eleve'];
        $reponseAjoutCommanditaire=$bdd->prepare("INSERT INTO commanditaire (type, id_eleve)
        VALUES ('eleve','$id')");
        $reponseAjoutCommanditaire->execute();

        $r3=$bdd->prepare("SELECT id_commanditaire FROM commanditaire JOIN eleve ON commanditaire.id_eleve = eleve.id_eleve WHERE eleve.contact = '$commanditaire'");
        $r3->execute();
        $r4 = $r3->fetch();
        $idCommanditaire = $r4['id_commanditaire'];
      }
      else{
        $idCommanditaire = $r2['id_commanditaire'];
      }
    }

    $reponseAjoutGroupe=$bdd->prepare("INSERT INTO projet (nom, nb_max, deadline, semestre, description, pj, id_commanditaire, id_matiere, id_promo)
    VALUES ('$nomProjet',$tailleGroupe, '$date' ,$numeroSemestre,'$description','$fichierComplementaire',$idCommanditaire,$idMatiere,$idPromo) ");

    $reponseAjoutGroupe->execute();

    header('Content-Type: application/json; charset=UTF-8');
    header('HTTP/1.1 200 OK');
    echo $response;

}
else {
    http_response_code(404);
}

?>
