# WebS2-IMAC

**Présentation :** 

En pleine période de projet, alors que les sujets s’accumulent, nous avons tous suivi les étapes habituelles : Recherche de groupe, création de fichiers excels sur les drive IMAC, les questions fusent : «Combien par groupe?», «Est-ce qu’on garde les même ?», «Quand est la deadline, déjà?», «Qui a de la place?», etc. 

Notre projet consiste à trouver une solution pour que toutes ces questions trouvent réponse le plus rapidement possible, et le plus simplement possible : PROJIMAC. L’objectif est de recenser tous les projets en cours, à venir, ou passés, pour faciliter le plus possible leur organisation. 

Les projets y sont enregistrés, avec leurs groupes, et les groupes avec leurs élèves et leur capacités. 
Dans le but de faciliter au maximum l’organisation, les sujets et autres documents ou liens utiles sont aussi mis à disposition directement depuis le site, et accessibles pour les élèves. 

Une base de donnée contient les projets, les professeurs et élèves, les contacts, et tous les autres détails utiles au bon fonctionnement du site comme nous le verrons dans la partie du rapport dédiée. 

**Répartition :**

Pour ce qui est de la répartition, nous avons joué sur les points forts de chacuns. Nous nous sommes organisés à l’aide d’un drive, d’un GIT (gitLab) et d’un Trello, plateforme utilisée pour répartir et visualiser les tâches. Si les rôles étaient fixés, la plupart des choses ont été réalisées en collaboration les uns avec les autres.

**Rôles principaux :** 

* Théo : Front end, PHP,  JS
* Esther : MCD, JS, PHP 
* Sterenn : JS, PHP 
* Benjamin : PHP, SQL 
* Simon : PHP, Compte rendu | **Chef de Projet**

**Planning :** 

Pour nous organiser au mieux et pouvoir respecter les deadlines, nous avons utilisé deux outils principaux : GitLab et Trello, nous permettant d’être optimum dans la programmation ainsi que dans la répartition des tâches et la visualisation des échéances. 
Nous avons d’abord établi une maquette papier ainsi qu’un plan sur un document partagé Google Drive, et dès la première semaine la programmation pouvait commencer. 
Les deadlines du sujet ont étées respectées sans encombre : Pitch le 11 mai, MCD le 1- avril, hebergement le 3 mai… <br>
[Accéder au Trello](https://trello.com/b/t3AXSKOS/projimac)

**Front End | Architecture Graphique :** 

La première étape de la réalisation graphique du site a été d’effectuer une veille de différentes plateformes collaboratives. Nous avons ainsi pu mettre en évidence certaines tendances comme l’utilisation de plus en plus fréquente de dégradés à trois couleurs et de formes arrondies.
Le site internet reposant essentiellement sur des formulaires, il fallait donc effectuer un travail de réflexion afin de les rendre à la fois fonctionnels, complets, compréhensibles pour l’utilisateur et attrayants.
L’affichage des différents projets se devait aussi d’être ergonomique, de donner toutes les informations nécessaires, tout cela sans surcharger la page. Nous avons alors décidé de nous concentrer sur l’affichage du nom du projet, du commanditaire, de la date de rendu et du semestre. Les autres informations seront disponibles en cliquant sur un bouton “En savoir plus” tandis que la matière sera symbolisée par un icône. 
Avec ces premières idées et réflexions, nous avons pu travailler sur une première maquette papier.

Après avoir amélioré cette maquette et ajouté les idées des uns et des autres, nous avons commencé la maquette finale.
Pour cela, nous avons utilisé la plateforme Figma, un site internet permettant de désigner de manière collaborative. Cela nous a permis de donner plus de profondeur à nos maquettes papier, d’y ajouter la couleur, et de clairement finir de définir les fonctionnalités du site. <br>

[Accéder à la maquette Figma](https://www.figma.com/file/KxASiJLvSJdByXhSqBLp2f/PROJ-IMAC?node-id=0%3A1)

La palette graphique est de trois couleurs : le beige, le rose et le violet, offrant un dégradé harmonieux et assez “flashy”.  


Quant au logo, il est uniquement typographique et reprend le nom du site Internet : PROJIMAC. La police utilisée est le Roboto Slab en deux graisses : light et bold. Enfin, le logo reprend les couleurs du site sous forme de dégradé.


Vous pouvez retrouver notre logo en bas de toutes les pages de ce rapport ainsi que sur la première. 

Une fois la maquette approuvée par toute l’équipe, nous avons pu nous lancer dans l’intégration. Nous avons rapidement choisi d’utiliser le framework “Bootstrap 4”. Cela nous a permis de simplifier notre code HTML et CSS mais aussi de créer plus facilement des fenêtres modal, des accordéons et des formulaires modernes.

**Architecture :** 

Tout d’abord, le MCD. Nous avons utilisé Merise pour le créer, et voici sa version finale. L’objectif était d’avoir une base de donnée efficace et sans erreur de sémantique (redondance, par exemple). 
Créer le MCD sur Merise était également un gain de temps, le logiciel permet, entre autre, de convertir le MCD en base de donnée SQL. Nous avons donc pu l’importer rapidement.

**Arborescence :**

Il était important pour nous de créer une arborescence logique pour le projet. Cela permet une meilleure lisibilité et une meilleure compréhension entre les membres du groupe. On y retrouve plusieurs parties, dont le CSS avec notamment des fichiers Bootstrap, un framework simplifiant la mise en page. Une autre partie comprend les fichiers Javascript et une dernière, nommé “traitement” comprend les fichiers PHP utile lorsque des actions sont réalisées sur le site.

**Tâches et fonctionnalités :** 

Toutes les fonctionnalités implémentées visent à pouvoir : 1. Visualiser les projets et leurs groupes ; 2. Créer de nouveaux projets et groupes ; 3. Modifier les projets, leurs informations, ainsi que les groupes qui s’y sont inscrits. V
oici la liste des fonctionnalités que nous avons implémentées pour arriver à ce résultat : 
* Affichage des projets et des groupes 
* Formulaire d’ajout de projet 
* Boutons “ en savoir plus “, Modifier, voir groupes
* Pop-up de modification de projet, de création de groupe, de suppression de projet
* Filtre des projets selon leur spécificités : Matière, semestre… 
* BDD Stockant les élèves et professeurs ainsi que leur coordonnées, les groupes etc. 

**Un exemple de fonctionnalité : Affichage et tri des projets**

L’affichage et le tri des projets sont des fonctionnalités étroitement liées car elles sont gérées dans une seule et même fonction javascript. Cette fonction peut prendre en paramètre plusieurs données telles qu’un mot/caractère pour trier les noms de projets, un semestre, une matière ou encore un paramètre de tri (date de rendu croissante/décroissante, projet terminé ou en cours). 
Au chargement de la page les paramètres sont tous nuls ce qui a pour conséquence d’afficher l’ensemble des projets.
Lorsqu’un changement est effectué dans l’interface de tri : mot tapé dans la barre de recherche, matière ou semestre sélectionné, etc; la fonction est alors de nouveau appelée avec les nouveaux paramètres.
Ces paramètres sont alors stockés dans un tableau associatif puis envoyés avec la méthode POST dans notre fichier php de traitement (après avoir été transformés au format JSON). 

```
let params = {}; //tableau associatif
    params.recherche = recherche;
    params.tri = tri;
    params.semestre = semestre;
    params.matiere = matiere
 
    fetch('traitement/affichageProjets.php', {
        method: 'POST',
        body:JSON.stringify(params)
    })
```


Après un certain nombre de vérifications dans le fichier php, le fichier JSON est décodé. Ensuite, nous avons réalisé plusieurs conditions : la requête SQL est donc différente en fonction du tri effectué par l’utilisateur.
Par exemple :

```
if($tri == "dateAsc"){
    $projets=$bdd->prepare("SELECT* FROM projet WHERE nom LIKE '%$recherche%' AND semestre LIKE '%$semestre%' ORDER BY deadline ASC");
}
```


Cette requête nous renverra tous les projets dont le nom comporte la chaîne de caractère “recherche” et dont le semestre est celui sélectionné. Les résultats seront alors ordonnés selon leur date de rendu et ce de manière croissante.

```
elseif($tri == "termines"){
    $projets=$bdd->prepare("SELECT* FROM projet WHERE deadline <= NOW() AND nom LIKE '%$recherche%' AND semestre LIKE '%$semestre%' ORDER BY deadline");
}
```


Cette requête nous permettra d’afficher seulement les projets “terminés”, c’est-à-dire ceux dont la date de rendu est dépassée. 

Les résultats sont alors stockés sous la forme d’un tableau par projet. Chaque tableau est rempli avec les différentes données nécessaires à l’affichage (nom du projet, commanditaire, icône, deadline, etc).
Le tableau comprenant l’ensemble des projets est ensuite encodé en JSON et renvoyé à la fonction Javascript qui va s’occuper d’afficher les résultats en HTML et de modifier le DOM.

**Gestion du projet :** 

Pour faciliter la mise en place des différentes fonctionnalités, nous avons travaillé de la façon la plus modulaire possible. Quasiment chaque fonctionnalité possède un fichier propre, permettant de la retravailler, la modifier, en préservant toujours  toutes les autres. Les méthodes que nous avons utilisées …

**Problèmes rencontrés et résultats :** 

Durant la réalisation du projet, notre équipe s’est assez bien articulée et nous avons pu avancer de manière plutôt continue, sans rester bloqués trop longtemps sur une difficulté particulière. Néanmoins, nous avons tout de même étés ralentis par certains problèmes : 
* Le fonctionnement du REST, et l’ajout de projet sans avoir à refresh la page. 
* La récupération de toutes les informations pour le tri 
* Des problèmes liés à Git et/ou Gitkraken pour l’envoie ou le téléchargement des fichiers.


Dans tous les cas, après avoir testé plusieurs possibilités et parfois demandé de l’aide à nos camarades, nous avons réussi à trouver des solutions adéquates.

Nous sommes satisfaits du travail effectué et espérons que ce site pourra trouver une place dans l’organisation future des élèves de l’IMAC. 

**Post - Mortem :** 

Nous avons encore des idées d’améliorations potentielles pour PROJIMAC, et même si le site en tant que tel fonctionne comme nous l’avions prévu, plusieurs possibilités restent à considérer. Par exemple, ajouter plus de liens au groupe (Par exemple un GIT ou autre), ou permettre de donner des rôles. 
L’autre problème majeur du site en tant que tel est l’absence de session. Nous voulions que les élèves puissent utiliser la plateforme pour des projets faits entre eux : Donc, il est normal que n’importe qui puisse créer un projet. Mais n’importe qui peut aussi les supprimer, ou assigner des gens dans un groupe. L’utilisation de sessions permettrait non seulement d’augmenter la sécurité du site, mais aussi d’en augmenter l’intuitivité (plus de select interminable avec tous les élèves et les professeurs, mais seulement avec la ou les personnes concernées par la session). 


