-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mar. 12 mai 2020 à 14:52
-- Version du serveur :  5.7.24
-- Version de PHP :  7.2.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `projimac`
--

-- --------------------------------------------------------

--
-- Structure de la table `commanditaire`
--

DROP TABLE IF EXISTS `commanditaire`;
CREATE TABLE IF NOT EXISTS `commanditaire` (
  `id_commanditaire` int(11) NOT NULL AUTO_INCREMENT,
  `type` varchar(40) NOT NULL,
  `id_prof` int(11) DEFAULT NULL,
  `id_eleve` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_commanditaire`),
  KEY `COMMANDITAIRE_PROFESSEUR_FK` (`id_prof`),
  KEY `COMMANDITAIRE_ELEVE0_FK` (`id_eleve`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `commanditaire`
--

INSERT INTO `commanditaire` (`id_commanditaire`, `type`, `id_prof`, `id_eleve`) VALUES
(1, 'professeur', 1, NULL),
(2, 'professeur', 2, NULL),
(3, 'professeur', 4, NULL),
(4, 'professeur', 3, NULL),
(9, 'eleve', NULL, 1),
(10, 'professeur', 5, NULL),
(11, 'eleve', NULL, 8),
(12, 'eleve', NULL, 2),
(13, 'professeur', 6, NULL);

-- --------------------------------------------------------

--
-- Structure de la table `eleve`
--

DROP TABLE IF EXISTS `eleve`;
CREATE TABLE IF NOT EXISTS `eleve` (
  `id_eleve` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(40) NOT NULL,
  `prenom` varchar(40) NOT NULL,
  `contact` varchar(40) NOT NULL,
  PRIMARY KEY (`id_eleve`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `eleve`
--

INSERT INTO `eleve` (`id_eleve`, `nom`, `prenom`, `contact`) VALUES
(1, 'Wayne', 'Bruce', 'bruce.wayne@etud.u-pem.fr'),
(2, 'Kent', 'Clark', 'clark.kent@etud.u-pem.fr'),
(3, 'Kyle', 'Selina', 'selina.kyle@etud.u-pem.fr'),
(4, 'Grayson', 'Dick', 'dick.grayson@etud.u-pem.fr'),
(5, 'Gordon ', 'James', 'james.gordon@etud.u-pem.fr'),
(6, 'Gordon', 'Barbara', 'barbara.gordon@etud.u-pem.fr'),
(7, 'Banner', 'Bruce', 'bruce.banner@etud.u-pem.fr'),
(8, 'Parker', 'Peter', 'peter.parker@etud.u-pem.fr'),
(9, 'Stark', 'Tony', 'tony.stark@@etud.u-pem.fr'),
(10, 'Rodger', 'Steeve', 'steeve.rodger@etud.u-pem.fr'),
(11, 'Barbara', 'Gordon', 'barbara.gordon1@etud.u-pem.fr'),
(12, 'Romanoff', 'Natasha', 'natasha.romanoff@etud.u-pem.fr'),
(13, 'Scott', 'Lang', 'scott.lang@etud.u-pem.fr'),
(14, 'Yelena', 'Belova', 'yelena.belova@etud.u-pem.fr'),
(15, 'Carol', 'Danvers', 'carol.danvers@etud.u-pem.fr'),
(16, 'Taneleer', 'Tivan', 'taneleer.tivan@etud.u-pem.fr'),
(17, 'Erik', 'Selvig', 'erik.selvig@etud.u-pem.fr'),
(18, 'Stephen', 'Strange', 'stephen.strange@etud.u-pem.fr'),
(19, 'Ebony ', 'Maw', 'ebony .maw@etud.u-pem.fr'),
(20, 'Corvus', 'Glaive', 'corvus.glaive@etud.u-pem.fr'),
(21, 'Erik', 'Killmonger', 'erik.killmonger@etud.u-pem.fr'),
(22, 'Sam ', 'Wilson', 'sam .wilson@etud.u-pem.fr'),
(23, 'Goose', 'Cat', 'goose.cat@etud.u-pem.fr'),
(24, 'Henry Jonathan', 'Pym', 'henry jonathan.pym@etud.u-pem.fr'),
(25, 'Clint ', 'Barton', 'clint .barton@etud.u-pem.fr'),
(26, 'Howard', 'Stark', 'howard.stark@etud.u-pem.fr'),
(27, 'Bruce', 'Banner', 'bruce.banner@etud.u-pem.fr'),
(28, 'Melina', 'Vostokovna', 'melina.vostokovna@etud.u-pem.fr'),
(29, 'Nick', 'Fury', 'nick.fury@etud.u-pem.fr'),
(30, 'Odin', 'Borson', 'odin.borson@etud.u-pem.fr'),
(31, 'Peggy ', 'Carter', 'peggy .carter@etud.u-pem.fr'),
(32, 'Phil', 'Coulson', 'phil.coulson@etud.u-pem.fr'),
(33, 'Jean', 'Grey', 'jean.grey@etud.u-pem.fr'),
(34, 'Alexei ', 'Shostakov', 'alexei .shostakov@etud.u-pem.fr'),
(35, 'Wanda', 'Maximoff', 'wanda.maximoff@etud.u-pem.fr'),
(36, 'Herman', 'Schultz', 'herman.schultz@etud.u-pem.fr'),
(37, 'Peter', 'Quill', 'peter.quill@etud.u-pem.fr'),
(38, 'Anthony', 'Masters', 'anthony.masters@etud.u-pem.fr'),
(39, 'James ', 'Rhodes', 'james .rhodes@etud.u-pem.fr'),
(40, 'Nadia', 'Pym', 'nadia.pym@etud.u-pem.fr'),
(41, 'Bucky', 'Barnes', 'bucky.barnes@etud.u-pem.fr'),
(42, 'Blackagar', 'Boltagon', 'blackagar.boltagon@etud.u-pem.fr'),
(43, 'Tyrone', 'Johnson', 'tyrone.johnson@etud.u-pem.fr'),
(44, 'Tandy', 'Bowen', 'tandy.bowen@etud.u-pem.fr'),
(45, 'Matt ', 'Murdock', 'matt .murdock@etud.u-pem.fr'),
(46, 'Elektra', 'Natchios', 'elektra.natchios@etud.u-pem.fr'),
(47, 'Foggy', 'Nelson', 'foggy.nelson@etud.u-pem.fr'),
(48, 'Norman ', 'Osborn', 'norman .osborn@etud.u-pem.fr'),
(49, 'Jessica', 'Jones', 'jessica.jones@etud.u-pem.fr'),
(50, 'Karen', 'Page', 'karen.page@etud.u-pem.fr'),
(51, 'Billy', 'Russo', 'billy.russo@etud.u-pem.fr');

-- --------------------------------------------------------

--
-- Structure de la table `enseigne_une`
--

DROP TABLE IF EXISTS `enseigne_une`;
CREATE TABLE IF NOT EXISTS `enseigne_une` (
  `id_prof` int(11) NOT NULL,
  `id_matiere` int(11) NOT NULL,
  PRIMARY KEY (`id_prof`,`id_matiere`),
  KEY `enseigne_une_MATIERE0_FK` (`id_matiere`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `enseigne_une`
--

INSERT INTO `enseigne_une` (`id_prof`, `id_matiere`) VALUES
(2, 1),
(4, 1),
(1, 2),
(5, 3),
(6, 4),
(2, 5),
(2, 6),
(3, 7);

-- --------------------------------------------------------

--
-- Structure de la table `etudie_en`
--

DROP TABLE IF EXISTS `etudie_en`;
CREATE TABLE IF NOT EXISTS `etudie_en` (
  `id_eleve` int(11) NOT NULL,
  `id_promo` int(2) NOT NULL,
  PRIMARY KEY (`id_promo`,`id_eleve`),
  KEY `etudie_en_PROMOTION_FK` (`id_eleve`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `etudie_en`
--

INSERT INTO `etudie_en` (`id_eleve`, `id_promo`) VALUES
(1, 1),
(2, 1),
(3, 1),
(4, 1),
(5, 2),
(6, 2),
(7, 2),
(8, 2),
(9, 3),
(10, 3),
(11, 3),
(12, 4),
(13, 1),
(14, 2),
(15, 1),
(16, 1),
(17, 2),
(18, 4),
(19, 4),
(20, 2),
(21, 3),
(22, 2),
(23, 2),
(24, 4),
(25, 4),
(26, 1),
(27, 1),
(28, 1),
(29, 3),
(30, 1),
(31, 1),
(32, 4),
(33, 3),
(34, 1),
(35, 3),
(36, 4),
(37, 1),
(38, 4),
(39, 1),
(40, 1),
(41, 1),
(42, 2),
(43, 1),
(44, 2),
(45, 4),
(46, 4),
(47, 1),
(48, 1),
(49, 3),
(50, 3),
(51, 1);

-- --------------------------------------------------------

--
-- Structure de la table `groupe`
--

DROP TABLE IF EXISTS `groupe`;
CREATE TABLE IF NOT EXISTS `groupe` (
  `id_groupe` int(11) NOT NULL AUTO_INCREMENT,
  `projet` varchar(40) NOT NULL,
  `nb_membre` int(11) NOT NULL,
  `membres` varchar(100) NOT NULL,
  `id_projet` int(11) NOT NULL,
  PRIMARY KEY (`id_groupe`),
  KEY `GROUPE_PROJET_FK` (`id_projet`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `groupe`
--

INSERT INTO `groupe` (`id_groupe`, `projet`, `nb_membre`, `membres`, `id_projet`) VALUES
(1, 'IMAC WARS 2', 1, '', 1),
(2, 'IMAC WARS 2', 2, '', 1),
(3, 'JEUX 3D', 3, '', 3),
(4, 'PROJIMAC', 4, '', 4);

-- --------------------------------------------------------

--
-- Structure de la table `matiere`
--

DROP TABLE IF EXISTS `matiere`;
CREATE TABLE IF NOT EXISTS `matiere` (
  `id_matiere` int(11) NOT NULL AUTO_INCREMENT,
  `nom_matiere` varchar(40) NOT NULL,
  `icone` varchar(40) NOT NULL,
  `tag` varchar(30) NOT NULL,
  PRIMARY KEY (`id_matiere`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `matiere`
--

INSERT INTO `matiere` (`id_matiere`, `nom_matiere`, `icone`, `tag`) VALUES
(1, 'Programmation / Algorithmique', 'fas fa-laptop-code', 'prog_algo'),
(2, 'Anglais', 'fa fa-globe', 'anglais'),
(3, 'Mathématiques', 'fas fa-calculator', 'maths'),
(4, 'Traitement du Signal', 'fas fa-wave-square', 'traitement_signal'),
(5, 'PHP', 'fas fa-code', 'php'),
(6, 'Architecture Logicielle', 'fas fa-laptop-code', 'archi_logicielle'),
(7, 'Synthèse d\'image', 'fas fa-cube', 'synthese_image'),
(8, 'Communication', 'fas fa-comments', 'communication'),
(9, 'Web', 'fas fa-wifi', 'web'),
(10, 'Histoire de l\'Art', 'fas fa-palette', 'histoire_art');

-- --------------------------------------------------------

--
-- Structure de la table `membre_d_un`
--

DROP TABLE IF EXISTS `membre_d_un`;
CREATE TABLE IF NOT EXISTS `membre_d_un` (
  `id_groupe` int(11) NOT NULL,
  `id_eleve` int(11) NOT NULL,
  PRIMARY KEY (`id_groupe`,`id_eleve`),
  KEY `membre_d_un_ELEVE0_FK` (`id_eleve`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `membre_d_un`
--

INSERT INTO `membre_d_un` (`id_groupe`, `id_eleve`) VALUES
(1, 1),
(4, 1),
(2, 2),
(4, 2),
(2, 3),
(4, 3),
(4, 4),
(3, 5),
(3, 6),
(3, 7);

-- --------------------------------------------------------

--
-- Structure de la table `professeur`
--

DROP TABLE IF EXISTS `professeur`;
CREATE TABLE IF NOT EXISTS `professeur` (
  `id_prof` int(11) NOT NULL AUTO_INCREMENT,
  `nom_prof` varchar(40) NOT NULL,
  `contact` varchar(40) NOT NULL,
  PRIMARY KEY (`id_prof`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `professeur`
--

INSERT INTO `professeur` (`id_prof`, `nom_prof`, `contact`) VALUES
(1, 'Templier', 'templier@u-pem.fr'),
(2, 'Cherrier', 'cherrier@u-pem.fr'),
(3, 'Biri', 'biri@u-pem.fr'),
(4, 'Vincent', 'vincent@u-pem.fr'),
(5, 'Novelli', 'novelli@u-pem.fr'),
(6, 'Chevreuil', 'chevreuil@u-pem.fr');

-- --------------------------------------------------------

--
-- Structure de la table `projet`
--

DROP TABLE IF EXISTS `projet`;
CREATE TABLE IF NOT EXISTS `projet` (
  `id_projet` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(60) NOT NULL,
  `nb_max` int(11) NOT NULL,
  `deadline` date NOT NULL,
  `semestre` int(11) NOT NULL,
  `description` varchar(100) NOT NULL,
  `pj` varchar(100) NOT NULL,
  `id_commanditaire` int(11) NOT NULL,
  `id_matiere` int(11) NOT NULL,
  `id_promo` int(2) NOT NULL,
  PRIMARY KEY (`id_projet`),
  KEY `PROJET_COMMANDITAIRE_FK` (`id_commanditaire`),
  KEY `PROJET_MATIERE0_FK` (`id_matiere`),
  KEY `PROJET_PROMOTION_FK` (`id_promo`)
) ENGINE=InnoDB AUTO_INCREMENT=46 DEFAULT CHARSET=utf8;

--
-- Déchargement des données de la table `projet`
--

INSERT INTO `projet` (`id_projet`, `nom`, `nb_max`, `deadline`, `semestre`, `description`, `pj`, `id_commanditaire`, `id_matiere`, `id_promo`) VALUES
(1, 'IMAC WARS 2', 3, '2020-06-01', 2, 'projet de jeu fin de semestre', 'https://drive.google.com/file/d/1NTOq2wXxeu_mt1eO6iMeHOdxX-RMHAU-/view', 4, 7, 3),
(2, 'THE LOVE BOAT', 4, '2020-03-21', 2, 'doublage', '', 1, 2, 3),
(3, 'JEUX 3D', 3, '2020-05-20', 4, '', '', 4, 7, 2),
(4, 'BDD', 5, '2020-06-06', 2, 'création d\'un site basé sur une bdd', '', 2, 5, 3),
(40, 'La Bite', 4, '2020-05-28', 2, 'c\'est la bite', '', 9, 10, 3),
(41, 'Nouveau projet trop bien par un élève', 6, '2020-06-05', 6, 'C\'est un élève qui le propose', '', 11, 9, 4),
(42, 'Nouveau projet trop bien par un élève 2', 6, '2020-06-05', 6, 'C\'est un élève qui le propose 2  ', '', 12, 9, 4),
(43, 'Nouveau projet trop bien par ', 6, '2020-06-05', 6, 'C\'est un professeur qui le propose 1  ', '', 9, 1, 4),
(44, 'Nouveau projet trop bien par ', 6, '2020-06-05', 6, 'C\'est un professeur qui le propose 1  ', '', 1, 1, 4),
(45, 'Nouveau projet trop bien par ', 6, '2020-06-05', 6, 'C\'est un professeur qui le propose 1  ', '', 10, 1, 4);

-- --------------------------------------------------------

--
-- Structure de la table `promotion`
--

DROP TABLE IF EXISTS `promotion`;
CREATE TABLE IF NOT EXISTS `promotion` (
  `id_promo` int(2) NOT NULL AUTO_INCREMENT,
  `nom` varchar(12) CHARACTER SET utf8 NOT NULL,
  PRIMARY KEY (`id_promo`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `promotion`
--

INSERT INTO `promotion` (`id_promo`, `nom`) VALUES
(1, 'IMAC 2020'),
(2, 'IMAC 2021'),
(3, 'IMAC 2022'),
(4, 'IMAC 2023');

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `commanditaire`
--
ALTER TABLE `commanditaire`
  ADD CONSTRAINT `COMMANDITAIRE_ELEVE0_FK` FOREIGN KEY (`id_eleve`) REFERENCES `eleve` (`id_eleve`),
  ADD CONSTRAINT `COMMANDITAIRE_PROFESSEUR_FK` FOREIGN KEY (`id_prof`) REFERENCES `professeur` (`id_prof`);

--
-- Contraintes pour la table `enseigne_une`
--
ALTER TABLE `enseigne_une`
  ADD CONSTRAINT `enseigne_une_MATIERE0_FK` FOREIGN KEY (`id_matiere`) REFERENCES `matiere` (`id_matiere`),
  ADD CONSTRAINT `enseigne_une_PROFESSEUR_FK` FOREIGN KEY (`id_prof`) REFERENCES `professeur` (`id_prof`);

--
-- Contraintes pour la table `etudie_en`
--
ALTER TABLE `etudie_en`
  ADD CONSTRAINT `etudie_en_ELEVE0_FK` FOREIGN KEY (`id_promo`) REFERENCES `promotion` (`id_promo`),
  ADD CONSTRAINT `etudie_en_PROMOTION_FK` FOREIGN KEY (`id_eleve`) REFERENCES `eleve` (`id_eleve`);

--
-- Contraintes pour la table `groupe`
--
ALTER TABLE `groupe`
  ADD CONSTRAINT `GROUPE_PROJET_FK` FOREIGN KEY (`id_projet`) REFERENCES `projet` (`id_projet`);

--
-- Contraintes pour la table `membre_d_un`
--
ALTER TABLE `membre_d_un`
  ADD CONSTRAINT `membre_d_un_ELEVE0_FK` FOREIGN KEY (`id_eleve`) REFERENCES `eleve` (`id_eleve`),
  ADD CONSTRAINT `membre_d_un_GROUPE_FK` FOREIGN KEY (`id_groupe`) REFERENCES `groupe` (`id_groupe`);

--
-- Contraintes pour la table `projet`
--
ALTER TABLE `projet`
  ADD CONSTRAINT `PROJET_COMMANDITAIRE_FK` FOREIGN KEY (`id_commanditaire`) REFERENCES `commanditaire` (`id_commanditaire`),
  ADD CONSTRAINT `PROJET_MATIERE0_FK` FOREIGN KEY (`id_matiere`) REFERENCES `matiere` (`id_matiere`),
  ADD CONSTRAINT `PROJET_PROMOTION_FK` FOREIGN KEY (`id_promo`) REFERENCES `promotion` (`id_promo`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
