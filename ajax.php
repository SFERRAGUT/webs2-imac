<?php

$method=strtolower($_SERVER['REQUEST_METHOD']);

if($method == 'post'){
    $json = file_get_contents('php://input'); 
    $data = json_decode($json, TRUE); 

    $nomProjet = $data['nomProjet']; 
    $categorie = $data['categorie']; 
    $description = $data['description']; 
    $commanditaire = $data['commanditaire']; 
    $tailleGroupe = $data['tailleGroupe']; 
    $promo = $data['promo'];
    $semestre= $data['semestre']; 
    $fichierComplementaire = $data['fichierComplementaire']; 
    

    $response = json_encode(array(
    'nomProjet' => $nomProjet, 
    'categorie' => $categorie,
    'description' => $description,
    'commanditaire' => $commanditaire, 
    'tailleGroupe' => $tailleGroupe,
    'promo' => $promo,
    'semestre' => $semestre, 
    'fichierComplementaire' => $fichierComplementaire,
    ));
    
    header('Content-Type: application/json; charset=UTF-8');
    header('HTTP/1.1 200 OK');
    echo $response;
}
else {
    http_response_code(404);
}
?>

