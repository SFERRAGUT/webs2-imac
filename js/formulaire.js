
var typeCommanditaireFormulaire;

function typeCommanditaire(){
  typeCommanditaireFormulaire = document.getElementById("selectType").value;
  //console.log(typeCommanditaireFormulaire);
  const commanditaireListe = document.getElementById('selectCommanditaire')
  var message = "";
  let params = {}; //tableau associatif
  params.typeCommanditaire = typeCommanditaireFormulaire;

  fetch('./traitement/formulaireProjetTraitement.php', {
    method: 'POST',
    body: JSON.stringify(params)
  })
  .then(response => response.json())
  .then(data => {
      data.forEach(function(e){
      if(typeCommanditaireFormulaire == 'professeur'){
        const info = "<option value ="+e.contact+">"+e.nom+"</option>";
        //console.log(info);
        message = message + info;
        //console.log(message);
      }
      else if(typeCommanditaireFormulaire == 'eleve'){
        const info = "<option value ="+e.contact+">"+e.nom+" "+e.prenom+"</option>";
        message = message + info;
      }
    })
    //console.log(message);
    commanditaireListe.innerHTML= message;
  })
}

var typeCommanditaireFormulaire2;

function typeCommanditaire2(){
  typeCommanditaireFormulaire2 = document.getElementById("selectType2").value;
  //console.log(typeCommanditaireFormulaire);
  const commanditaireListe = document.getElementById('selectCommanditaire2')
  var message = "";
  let params = {}; //tableau associatif
  params.typeCommanditaire = typeCommanditaireFormulaire2;

  fetch('./traitement/formulaireProjetTraitement.php', {
    method: 'POST',
    body: JSON.stringify(params)
  })
  .then(response => response.json())
  .then(data => {
      data.forEach(function(e){
      if(typeCommanditaireFormulaire2 == 'professeur'){
        const info = "<option value ="+e.contact+">"+e.nom+"</option>";
        //console.log(info);
        message = message + info;
        //console.log(message);
      }
      else if(typeCommanditaireFormulaire2 == 'eleve'){
        const info = "<option value ="+e.contact+">"+e.nom+" "+e.prenom+"</option>";
        message = message + info;
      }
    })
    //console.log(message);
    commanditaireListe.innerHTML= message;
  })
}

