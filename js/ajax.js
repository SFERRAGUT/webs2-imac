window.addEventListener('load', function() {
    affichageMatieres(),
    fonctionTri(),
    affichageTriProjets(null,null, null, null, null)
});


const b1 = document.getElementById('button');

b1.onclick = event => {
    event.preventDefault(); //fonction qui permet de ne pas raffraichir la page quand on clique
    const nomProjet = document.getElementById('nomProjet').value;
    const categorie = document.getElementById('selectCategorie').value;
    const description = document.getElementById('textAreaDescription').value;
    const typeCommanditaire = document.getElementById("selectType").value;
    const commanditaire = document.getElementById('selectCommanditaire').value;
    const tailleGroupe = document.getElementById('tailleGroupe').value;
    const promo = document.getElementById('selectPromo').value;
    const semestre = document.getElementById('selectSelect').value;
    var date = document.getElementById('dateRendu').value;
    const fichierComplementaire = document.getElementById('fichierComplementaire').value;

    var date = date.toString();


    let params = {}; //tableau associatif
    params.nomProjet = nomProjet;
    params.categorie = categorie;
    params.description = description;
    params.commanditaire = commanditaire;
    params.type = typeCommanditaire;
    params.tailleGroupe = tailleGroupe;
    params.promo = promo;
    params.semestre = semestre;
    params.fichierComplementaire = fichierComplementaire;
    params.date = date;

    fetch('traitement/ajouterProjet.php', {
        method: 'POST',
        body:JSON.stringify(params)
    })
    .then(response => response.json())
    .then(function(){
        affichageTriProjets(null,null, null, null, null);
    })
}

// Tri des résultats (ordre d'affichage)

 function fonctionTri(){
    var selectTri = document.getElementById('triResultats');
    var recherche = document.getElementById('champRecherche');

    selectTri.onchange = event => rechercheEtTri();
    recherche.onkeyup = event => rechercheEtTri();
    function rechercheEtTri(){
        var contenuRecherche = recherche.value;
        var value = selectTri.value;
        var semestre = document.getElementsByName('semestreProjet');
        var matiere = document.getElementsByName('categorieProjet');
        for(i = 0; i < semestre.length; i++) {
            if(semestre[i].checked)
            semestre = semestre[i].value;
        }
        if(semestre == 'all'){
            semestre = '';
        }

        for(i = 0; i < matiere.length; i++) {
            if(matiere[i].checked)
            matiere = matiere[i].value;
        }
        if(matiere == 'all'){
            matiere = '';
        }
        affichageTriProjets(contenuRecherche, value, semestre, matiere, null)
    };
}

// Tri des groupes
var b4 = document.getElementById('submit_filtre_groupe');

b4.onclick = event =>triGroupe();
function triGroupe() {

    var filtreGroupe = document.getElementsByName('categorieGroupe');
    var idProjet = document.getElementById('inputProjetId').value;

    for(i = 0; i < filtreGroupe.length; i++) {
        if(filtreGroupe[i].checked){
            filtreGroupe = filtreGroupe[i].value;
        }
    }
    affichageGroupe(idProjet, filtreGroupe);
}


function affichageTriProjets(recherche, tri, semestre, matiere, idProjet){
    let params = {}; //tableau associatif
        params.recherche = recherche;
        params.tri = tri;
        params.semestre = semestre;
        params.matiere = matiere;
        params.idProjet = idProjet;

    fetch('traitement/affichageProjets.php', {
        method: 'POST',
        body:JSON.stringify(params)
    })
		.then( response => response.json() )
		.then( data => {
            if(data.inGroup== true){
                document.getElementById('rappelProjet').innerHTML = "";
            }
            else{
                document.getElementById('resultats').innerHTML = "";
            }
            var i = 0;
            data.projets.forEach(function(projet){
                var boiteProjet = document.createElement("div");
                boiteProjet.classList.add('boiteResultat');
                boiteProjet.classList.add('actif');

                if(projet.projetTermine){
                    boiteProjet.classList.add('termine');
                }
                const contenu1 = "<div class='row'><div class='col-md-1'>";
                const contenu2 = "<i class='"+projet.icone+" form-control-feedback icon'></i></div>";
                const contenu3 = "<div class='col-md-9'><h2><span class='bold'>"+projet.nom+"</span> - S"+projet.semestre+"</h2>";
                const contenu4 = "<p><i>Commanditaire : "+projet.commanditaire+" - "+projet.contact+"</i></p>";
                const contenu5 = "<p>Date de rendu : "+projet.deadline+"</p>";
                const contenu6 = "<button class='btn btn-secondary' type='button' data-toggle='collapse' data-target='#resultat-"+projet.id+"' aria-expanded='false' aria-controls='collapseExample'>En savoir plus</button></div>";

                if(projet.projetTermine){
                    var contenu7 = '<div class="col-md-2 text-right"><span class="statut">Terminé</span></div></div>';
                }
                else{
                    var contenu7 = '<div class="col-md-2 text-right"><span class="statut">En cours</span></div></div>';
                }
                const contenu8 = '<div class="row"><div class="col-md-11 offset-md-1">';
                const contenu9 = "<div class='collapse' id='resultat-"+projet.id+"'>";
                const contenu10 = "<p><br><i>Description du projet :</i><br>"+projet.description+"</p>";
                const contenu11 = "<p><i>Promotion : </i> "+projet.promo+"</p>";
                const contenu12 = "<p><i>Composition des groupes :</i> "+projet.nbEleves+" étudiants maximum</p>";
                const contenu13 = "<form><button type='submit' class='btn btn-secondary' formaction='"+projet.pj+"'>Pièce Jointe</button>";
                if(data.inGroup== true){
                    var contenu14 = "";
                    var contenu15 = "";
                    var contenu16 = "";
                }
                else{
                    var contenu14 = "<button type='button' value ='"+projet.id+"' class='btn btn-secondary btnVoirGroupe'>Voir les groupes</button>";
                    var contenu15 = "<button type='button' class='btn btn-secondary btnModifierProjet' data-toggle='modal' data-target='#modalModifierProjet' value='"+projet.id+"'data-whatever='@getbootstrap' >Modifier</button>";
                    var contenu16 = "<button type='button' class='btn btn-secondary btnSuprimer'  data-toggle='modal' data-target='#modalSupprimerProjet' value='"+projet.id+"'>Supprimer le projet</button></form>";  
                }
                const contenu17 = "</div></div></div></div>";

                i++;
                boiteProjet.innerHTML =  contenu1 + contenu2 + contenu3+ contenu4 + contenu5 +contenu6 +contenu7 +contenu8 + contenu9 +contenu10 + contenu11 +contenu12+contenu13+contenu14+contenu15+contenu16+contenu17;
                if(data.inGroup== true){
                    document.getElementById('rappelProjet').appendChild(boiteProjet);
                }
                else{
                    document.getElementById('resultats').appendChild(boiteProjet);
                }

            })
            if(i==0){

                document.getElementById('resultats').innerHTML = "<div class='alert alert-primary' role='alert'>Aucun projet n'a été trouvé</div>";
            }
            document.getElementById('nbResult').innerHTML = i;


    })
}

// Affichage des Matières
function affichageMatieres(){
    fetch('traitement/filtreCategories.php', {
        method: 'POST',
    })
    .then( response => response.json() )
    .then( data => {
        document.getElementById('selectCat').innerHTML = "";
        var i = 1;
        var divSelect = document.createElement("div");
        divSelect.classList.add('form-check');
        const contenu1 = "<input class='form-check-input label-to-bold-if-checked' type='radio' name='categorieProjet' id='categorieProjet' value='all' checked>";
        const contenu2 = "<label class='form-check-label label-check' for='categorieProjet'>Tous les projets</label>";
        divSelect.innerHTML =  contenu1 + contenu2;
        document.getElementById('selectCat').appendChild(divSelect);

        data.matieres.forEach(function(matiere){
            var divSelect = document.createElement("div");
            divSelect.classList.add('form-check');
            const contenu1 = "<input class='form-check-input label-to-bold-if-checked' type='radio' name='categorieProjet' id='categorieProjet"+i+"' value='"+matiere.id+"'>";
            const contenu2 = "<label class='form-check-label label-check' for='categorieProjet"+i+"'>"+matiere.nom+"</label>";
            i++;
            divSelect.innerHTML =  contenu1 + contenu2;
            document.getElementById('selectCat').appendChild(divSelect);
        })
    })
}

// Tri des Projets 2
const btnFiltres = document.getElementById('submit_filtres');

btnFiltres.onclick = event => {
    event.preventDefault(); //fonction qui permet de ne pas raffraichir la page quand on clique
    var semestre = document.getElementsByName('semestreProjet');
    var matiere = document.getElementsByName('categorieProjet');

    for(i = 0; i < semestre.length; i++) {
        if(semestre[i].checked)
        semestre = semestre[i].value;
    }
    if(semestre == 'all'){
        semestre = '';
    }

    for(i = 0; i < matiere.length; i++) {
        if(matiere[i].checked)
        matiere = matiere[i].value;
    }
    if(matiere == 'all'){
        matiere = '';
    }

    //console.log(matiere);
    affichageTriProjets(null,null, semestre, matiere, null)
}

// Modifier Projet
function modifierProjet(idProjet) {
    //console.log(idProjet);
    let params = {}; //tableau associatif
    params.idProjet = idProjet;

    fetch('traitement/modifierProjet.php', {
        method: 'POST',
        body:JSON.stringify(params)
    })
    .then(response => response.json())
    .then(data => {
        document.getElementById("nomProjet2").value = data.nom;
        document.getElementById("textAreaDescription2").value = data.description;
        document.getElementById("tailleGroupe2").value = data.nbMax;
        document.getElementById("tailleGroupe2").min = data.nbMax;
        document.getElementById("dateRendu2").value = data.deadline;
        document.getElementById("fichierComplementaire2").value = data.pj;
        document.getElementById("idProjet2").value = data.id;
        document.getElementById("fichierComplementaire2").value = data.pj;

    })
}

var submitModif = document.getElementById('modifier');
submitModif.onclick = event => {
    event.preventDefault();
    let params = {};
        params.idProjet = document.getElementById("idProjet2").value;
        params.nom = document.getElementById("nomProjet2").value;
        params.description = document.getElementById("textAreaDescription2").value;
        params.nbMax = document.getElementById("tailleGroupe2").value;
        params.deadline = document.getElementById("dateRendu2").value;
        params.pj = document.getElementById("fichierComplementaire2").value;

    fetch('traitement/modifierProjetTraitement.php', {
        method: 'POST',
        body:JSON.stringify(params)
    })
    .then(function(){
        affichageTriProjets(null,null, null, null, null);
    })
}




window.setInterval(function(){
    let btnModifier = document.querySelectorAll('.btnModifierProjet');
    btnModifier.forEach((btn) => {
    btn.addEventListener("click", (event) => {
        var idProjet = btn.value;
        modifierProjet(idProjet);
    });
    });
}, 2000);


function supprimerProjet(idProjet){
    console.log("idProjet modifierProjet : "+idProjet);
    event.preventDefault();
    let params = {};
        params.idProjet = idProjet

    fetch('traitement/supprimerProjet.php', {
        method: 'POST',
        body:JSON.stringify(params)
    })
    .then(function(){
        affichageTriProjets(null,null, null, null, null);
    })
}

function ajoutValue(idProjet){
  console.log(idProjet);
  console.log(document.getElementById('supprimerProjetPopup'));
  document.getElementById('supprimerProjetPopup').value = idProjet;
}


window.setInterval(function(){
    let btnSuprimer = document.querySelectorAll('.btnSuprimer');
    btnSuprimer.forEach((btn) => {
    btn.addEventListener("click", (event) => {
        var idProjet = btn.value;
        ajoutValue(idProjet);
    });
    });
}, 1000);


var submitSuppression = document.getElementById('supprimerProjetPopup');
submitSuppression.onclick = event => {
    event.preventDefault();
    idProjet = document.getElementById('supprimerProjetPopup').value;

    let params = {};
        params.idProjet = idProjet

   fetch('traitement/supprimerProjet.php', {
       method: 'POST',
       body:JSON.stringify(params)
   })
   .then(function(){
       affichageTriProjets(null,null, null, null, null);
   })
}

// Nouvelle version de la fonction affichage groupe:
function affichageGroupe(idProjet, filtreGroupe){
    let params = {};
    params.idProjet = idProjet;
    params.filtreGroupe = filtreGroupe;

    const bGroupe = document.getElementById('structureBtnCreerGroupe');
    bGroupe.onclick = event => {
        fetch('traitement/creerNouveauGroupe.php', {
            method: 'POST',
            body:JSON.stringify(params)
        })
        .then(response => response.json())
        .then(data => {
            const selectModalNom = document.getElementById('selectModalNom');
            selectModalNom.innerHTML = "";
            
            var inputProjetId = document.createElement("input");
            inputProjetId.id="inputProjetId";
            inputProjetId.value=idProjet;
            inputProjetId.type="hidden";
            if(typeof(data[0]) !== 'undefined'){
                for (var i = 0; i < data[0].nb; i++) {
                    var modalEleve = document.createElement("div");
                    var membre = document.createElement("select");
                    modalEleve.classList.add("form-group");
                    membre.classList.add("custom-select");
                    var messageNom = "<option value ='null'> --- </option>";
                    membre.classList.add('choixEleve');
                    data.forEach(function(e){
                        const info = "<option value ="+e.contact+">"+e.nom+" "+e.prenom+"</option>";;
                        console.log(info);
                        messageNom = messageNom + info;
                    })
                    console.log(messageNom);
                    membre.innerHTML= messageNom;
                    modalEleve.appendChild(membre);
                    selectModalNom.appendChild(modalEleve);
                }
            }

            selectModalNom.appendChild(inputProjetId);


        })
    }

    fetch('traitement/affichageGroupeV2.php', {
        method: 'POST',
        body:JSON.stringify(params)
    })
    .then(response => response.json())
    .then(data => {
        document.getElementById('resultats').innerHTML = "";
        var i = 0;

        data.groupes.forEach(function(e){
            var boiteGroupe = document.createElement("div");
            var idGroupe = e.id_groupe;
            boiteGroupe.classList.add('boiteGroupe');
            const contenu1 = "<div class='row'><div class='col-md-9'>";
            const contenu2 = "<h2><span class='bold'>Groupe "+(i+1)+"</span></h2>";

            var contenuMembres ='';
            var nb_membres = 0;
            e.membres.forEach(function(membre){
                var infosMembre = '<p>'+membre.prenom+' '+membre.nom+' - <i>'+membre.contact+'</i></p>' ;
                contenuMembres += infosMembre;
                nb_membres ++;
            })
            contenuMembres += '</div>';
            if(nb_membres == data.taille_max){
                boiteGroupe.classList.add('complet');
                var contenu3 = "<div class='col-md-3 text-right'><span class='statut'>Complet </span><span class ='circle'>"+nb_membres+" </span></div></div>";
                var contenu4 = "<div class='row'><div class='col-md-11 offset-md-1 text-right'><button class='btn btn-light supprimerGroupe'value="+idGroupe+" type='button'>Supprimer</button><button class='btn btn-light' type='button' disabled>Rejoindre</button></div></div>";
            }
            else{
                var contenu3 = "<div class='col-md-3 text-right'><span class='statut'>Incomplet </span><span class ='circle'>"+nb_membres+" </span></div></div>";
                var contenu4 = "<div class='row'><div class='col-md-11 offset-md-1 text-right '><button class='btn btn-light supprimerGroupe' value="+idGroupe+" type='button'>Supprimer</button><button class='btn btn-light bouton-rejoindre' type='button' value="+idGroupe+" id='bouton-rejoindre' data-toggle='modal' data-target='#modalrejoindregroupe' data-whatever='@getbootstrap'>Rejoindre</button></div></div>";
                }


            i++;
            boiteGroupe.innerHTML =  contenu1 + contenu2 + contenuMembres+ contenu3 + contenu4;
            document.getElementById('resultats').appendChild(boiteGroupe);

        })
        if(i==0){
            document.getElementById('resultats').innerHTML = "<div class='alert alert-primary' role='alert'>Aucun groupe n'a été trouvé</div>";
        }
        var inputProjetId = document.createElement("input");
        inputProjetId.id="inputProjetId";
        inputProjetId.value=idProjet;
        inputProjetId.type="hidden";
        document.getElementById('resultats').appendChild(inputProjetId);
        document.getElementById("filtresProjets").style.display = "none";
        document.getElementById('partieTri').style.display = 'none';
        document.getElementById("filtreGroupes").style.display = 'block';
        document.getElementById('structureRecherche').style.display = 'none';
        document.getElementById('structureBtnRetour').style.display = 'block';
        document.getElementById('structureBtnCreerGroupe').style.display = 'block';
    })
    .then( function(){
        affichageTriProjets(null, null, null, null, idProjet)
    })
}

// TEST POUR REJOINDRE GROUPE
function rejoindreGroupe(idGroupe){
    let params = {};
    params.idGroupe = idGroupe;
    //console.log("coucou petite péruche !");

    fetch('traitement/rejoindreGroupe.php', {
          method: 'POST',
          body:JSON.stringify(params)
    })
    .then(response => response.json())
    .then(data => {
        const selectModalNom = document.getElementById('selectModalRejoindreNom');
        selectModalNom.innerHTML = "";

        var inputGroupeId = document.createElement("input");
        inputGroupeId.id="inputGroupeId";
        inputGroupeId.value=idGroupe;
        inputGroupeId.type="hidden";
        if(typeof(data[0]) !== 'undefined'){
            for (var i = 0; i < data[0].nb; i++) {
                var modalEleve = document.createElement("div");
                var membre = document.createElement("select");
                modalEleve.classList.add("form-group");
                membre.classList.add("custom-select");
                var messageNom = "<option value ='null'> --- </option>";
                membre.classList.add('choixEleve');
                data.forEach(function(e){
                    const info = "<option value ="+e.contact+">"+e.nom+" "+e.prenom+"</option>";;
                    console.log(info);
                    messageNom = messageNom + info;
                })
                console.log(messageNom);
                membre.innerHTML= messageNom;
                modalEleve.appendChild(membre);
                selectModalNom.appendChild(modalEleve);
            }
        }
    selectModalNom.appendChild(inputGroupeId);
    })
}

window.setInterval(function(){
    let btnRejoindre = document.querySelectorAll('.bouton-rejoindre');
    btnRejoindre.forEach((btn) => {
    btn.addEventListener("click", (event) => {
        var idGroupe = btn.value;
        //console.log(idGroupe);
        rejoindreGroupe(idGroupe);
    });
    });
}, 1000);


/////////////

// TEST POUR REJOINDRE GROUPE
function supprimerGroupe(idGroupe, idProjet){
    let params = {};
    params.idGroupe = idGroupe;
    //console.log("coucou petite péruche !");

    fetch('traitement/supprimerGroupe.php', {
        method: 'POST',
        body:JSON.stringify(params)
    })
    .then(function(){
        var filtre = 'all';
        affichageGroupe(idProjet, filtre);
    })
}



window.setInterval(function(){
    let btnSuprimerGroupe = document.querySelectorAll('.supprimerGroupe');
    btnSuprimerGroupe.forEach((btn) => {
    btn.addEventListener("click", (event) => {
        var idGroupe = btn.value;
        var idProjet = document.getElementById("inputProjetId").value;
        //console.log(idGroupe);
        supprimerGroupe(idGroupe, idProjet);
    });
    });
}, 1000);


/////////////


window.setInterval(function(){
    let btnVoirGroupe = document.querySelectorAll('.btnVoirGroupe');
    btnVoirGroupe.forEach((btn) => {
    btn.addEventListener("click", (event) => {
        var idProjet = btn.value;
        var filtre = 'all';
        affichageGroupe(idProjet, null);
    });
    });
}, 2000);

// Bouton Retour :
const b2 = document.getElementById('structureBtnRetour');

b2.onclick = event => {
    affichageTriProjets(null,null, null, null, null);
    document.getElementById('partieTri').style.display = 'flex';
    document.getElementById('structureRecherche').style.display = 'block';
    document.getElementById('structureBtnRetour').style.display = 'none';
    document.getElementById('structureBtnCreerGroupe').style.display = 'none';
    document.getElementById("filtresProjets").style.display = "block";
    document.getElementById("filtreGroupes").style.display = 'none';
    document.getElementById('rappelProjet').innerHTML = "";
}

const b3 = document.getElementById('validerGroupe');

b3.onclick = event => {
    event.preventDefault(); //fonction qui permet de ne pas raffraichir la page quand on clique
    const elevesgroupe=document.getElementsByClassName('choixEleve');
    var idProjet1 = document.getElementById('inputProjetId');
    idProjet1=idProjet1.value;
    var tab=[];
    var j=0;
    for (var i = 0; i < elevesgroupe.length; i++) {
      if (elevesgroupe[i].value!='null') {
        tab[j]=elevesgroupe[i].value;
        j++;
      }

    }

    let params = {}; //tableau associatif
    params.tabEleveGroupe = tab;
    params.idProj = idProjet1;
    //console.log(params);

    fetch('traitement/ajouterGroupe.php', {
        method: 'POST',
        body:JSON.stringify(params)
    })
    .then(function(){
      var filtre = 'all';
      affichageGroupe(idProjet1, filtre);
    })

}

const b5 = document.getElementById('rejoindre');

b5.onclick = event => {
  console.log("TEST !!!!!!!!!!!!!!!!!!!!!!");
  event.preventDefault(); //fonction qui permet de ne pas raffraichir la page quand on clique
  const elevesgroupe =document.getElementsByClassName('choixEleve');
  var idGroupe = document.getElementById('inputGroupeId');
  idGroupe=idGroupe.value;

  var idProjet = document.getElementById('inputProjetId');
  idProjet = idProjet.value;

  var tab=[];
  var j=0;
  for (var i = 0; i < elevesgroupe.length; i++) {
    if (elevesgroupe[i].value!='null') {
      tab[j]=elevesgroupe[i].value;
      j++;
    }
    console.log("ajax.js b5.onclick");
  }

  let params = {}; //tableau associatif
  params.tabEleveGroupe = tab;
  params.idGroupe = idGroupe;
  //params.idGroupe = idGroupe;
  //console.log(params);

  fetch('traitement/ajouterMembre.php', {
      method: 'POST',
      body:JSON.stringify(params)
  })
  .then(function(){
    var filtre = 'all';
    affichageGroupe(idProjet, filtre);
  })

}
